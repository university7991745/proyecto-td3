#ifndef TAREAS_H_
#define TAREAS_H_
#endif /* TAREAS_H_ */

#include "chip.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "funciones.h"
#include "timer.h"
#include "display.h"
#include "adc.h"

//Prototipos de funcion
void Teclado_aum(void);
void Teclado_dism(void);
void Teclado_prog(void);
void Teclado_modo(void);
uint32_t Tecla(void);
uint32_t puls_aum(void);
uint32_t puls_dism(void);
uint32_t puls_prog(void);
uint32_t puls_modo(void);
void fn_prog_modo (void);
void maq_est_menu_visual(void);
void maq_est_menu_prog(void);
