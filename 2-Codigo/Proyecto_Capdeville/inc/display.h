#ifndef DISPLAY_H_
#define DISPLAY_H_
#endif /* DISPLAY_H_ */

#include "chip.h"


//Define de variables
#define DISPLAY_RS        0, 9
#define DISPLAY_E         0, 0
#define DISPLAY_D4        0, 15
#define DISPLAY_D5        0, 16
#define DISPLAY_D6        0, 23
#define DISPLAY_D7        0, 24

//Prototipos de funciones
void fn_delay(uint32_t);
void init_4bits (void);
void cuatro_bits (void);
void limpio_display (void);
void activo_display (void);
void selecciono_modo (void);
void escribo_letra (uint32_t);
void posicion (uint32_t, uint32_t);
void escribo_display (char *, uint32_t, uint32_t);
void comienzo_init (void);
void return_home (void);
void reseteo_display (void);
