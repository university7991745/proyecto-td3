#ifndef MEMORIA_H_
#define MEMORIA_H_

#include "chip.h"

//Define de los pines
#define SDA1_PORT  0
#define SDA1_PIN  19
#define SCL1_PORT  0
#define SCL1_PIN  20

//Define de las posiciones de memoria
#define MEM_TEMP_PILETA      0
#define MEM_DIF_TEMP_ON      1
#define MEM_DIF_TEMP_OFF     2
#define MEM_ANTICONG         3
#define MEM_SOBRECAL         4
#define MEM_TEMP_MAX_PILETA  5
#define MEM_OFFSET_PILETA    6
#define MEM_HISTERESIS       7
#define MEM_PROTECCION_MAN   8
#define MEM_ESTADO_BOMBA     9

/* Dirección propia de la EEPROM */
#define SLAVE_ADDRESS	0x50

/* Posición donde se van a guardar los datos */
#define W_ADDRESS		0x00B0							//Posicion donde arranca la RAM, la siguiente posicion a utilizar seria 0x00C0


//Prototipos de funcion
void init_i2c(void);
void fn_escribo_EEPROM(uint32_t posicion, uint32_t buffer);
void fn_leo_EEPROM(uint32_t posicion, uint32_t * buffer);
void lectura_memoria_EEPROM(void);

#endif /* MEMORIA_H_ */
