#ifndef FUNCIONES_H_
#define FUNCIONES_H_

#endif /* FUNCIONES_H_ */

#include "timer.h"
#include "display.h"
#include "adc.h"
#include "defines_globales.h"

//Defines de salidas
#define BUZZER          2, 13
#define LED_ADVERTENCIA 2, 5
#define BOMBA           2, 12

//Define de pulsadores
#define BOTON_AUM        0, 25
#define BOTON_DISM       0, 26
#define BOTON_PROG       1, 30
#define BOTON_MODO       1, 31

//Define de las tareas
#define TEMP_DELAY_TASK   100

//Define de los sensores
#define PIN_SENSOR_COL  0, 2
#define PIN_SENSOR_PIL  0, 3

#ifndef ALGO
#define ALGO
//Define del struct con los datos de entrada
typedef struct configuraciones
{
	int32_t offset_pileta;
	int32_t temp_pileta;
	uint32_t estado_bomba;
	int32_t dif_temp_on;
	int32_t dif_temp_off;
	uint32_t anticong;
	uint32_t sobrecal;
	uint32_t temp_max_pileta;
	uint32_t histeresis;
	uint32_t proteccion_manual;
} my_configuraciones;
#endif

//Prototipos de funciones
void init_GPIO(void);
void pantalla_bienvenida(void);
void muestreo_temperatura(uint32_t punto_x, uint32_t punto_y, uint32_t dato);
void muestreo_bomba(uint32_t punto_x, uint32_t punto_y, uint32_t dato);
void muestreo_offset(uint32_t punto_x, uint32_t punto_y, int32_t dato);
void muestreo_proteccion(uint32_t punto_x, uint32_t punto_y, uint32_t dato);
void vuelta_menu(void);
uint32_t subo_teclado(uint32_t variable);
uint32_t bajo_teclado(uint32_t variable);
int32_t conversion(uint32_t sensor, uint32_t tipo, uint32_t punto_x, uint32_t punto_y);
int32_t fn_adquisicion_datos(uint16_t data);
void muestreo_sensores(uint32_t punto_x, uint32_t punto_y, int32_t sensor);
void fn_accionamiento_manual (void);
void accionamiento_bomba(uint32_t estado);
void fn_accionamiento_autom(void);
void accionamiento_led_advertencia(uint32_t estado);
void muestreo_menu(void);
void tomo_sensor_colector(void);
void tomo_sensor_pileta(void);
