#ifndef DEFINES_GLOBALES_H_
#define DEFINES_GLOBALES_H_
#endif /* DEFINES_GLOBALES_H_ */


//Define de los posibles botones
#define PULS_AUM       1
#define PULS_DIS       2
#define PULS_PROG      3
#define PULS_MODO      4

//Define de las posibilidades de la bomba
#define BOMBA_AUT      1
#define BOMBA_MAN      2
#define BOMBA_OFF      3

//Define de los limites de la funcion 1 y 2: Encendido / Apagado de Bomba
#define DIF_TEMP_ON    500                                                      //50.0 grados
#define DIF_TEMP_OFF   10                                                       //01.0 grados

//Define de los limites de la funcion 3, 4, 6: Anticongelamiento, Sobrecalentamiento, Temperatura de seteo maximo de pileta
#define TEMP_LIM_UP    850
#define TEMP_LIM_DOWN  1

//Define de los limites de la funcion 7: offset del sensor de la pileta
#define OFFSET_UP      200
#define OFFSET_DOWN    -200

//Define de las dos posibilidades de la funcion 9
#define DESHABILITADO  0
#define HABILITADO     1


//Define de si la temperatura del sensor a convertir es de pileta o de colector
#define SENSOR_PILETA   1
#define SENSOR_COLECTOR 2

//Define de la histeresis del anticongelamiento
#define ANTICONG_HISTERESIS 30

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Define de la maquina de estados: MENU VISUAL
#define MENU_TEMP      1
#define MENU_BOMBA     2
#define MENU_NULO      3

//Define de la maquina de estados: MENU_PROGRAMACION
#define MODO_1         1                                                        //Diferencia de temperatura para encender la bomba
#define MODO_2         2                                                        //Diferencia de temperatura para apagar la bomba
#define MODO_3         3                                                        //Temperatura de anticongelamiento
#define MODO_4         4                                                        //Temperatura de sobrecalentamiento
//Se supone que el modo 5 seria el seteo de la temperatura a mostrar, pero vamos a mostrar siempre el de la pileta y el del colector a la vez
#define MODO_6         6                                                        //Temperatura maxima permitida en el parametro del set point para el usuario
#define MODO_7         7                                                        //Correcciones que se hacen al sensor de temperatura
#define MODO_8         8                                                        //Diferencia entre el punto de sobrecalentamiento y el punto en que vuelva a realizar el control
#define MODO_9         9                                                        //Habilita el modo de sobrecalentamiento en el modo manual
#define MODO_REGRESAR  10
#define MODO_NULO      11

#define MODO_1_ENTRA   12                                                       //Entra al menu y permite modificar el valor del diferencial de temperatura para encender la bomba
#define MODO_2_ENTRA   13                                                       //Entra al menu y permite modificar el valor del diferencial de temperatura para apagar la bomba
#define MODO_3_ENTRA   14                                                       //Entra al menu y permite modificar la temperatura de anticongelamiento
#define MODO_4_ENTRA   15                                                       //Entra al menu y permite modificar la temperatura de sobrecalentamiento
//Se supone que el modo 5 seria el seteo de la temperatura a mostrar, pero vamos a mostrar siempre el de la pileta y el del colector a la vez
#define MODO_6_ENTRA   17                                                       //Entra al menu y permite modificar la temperatura maxima de la pileta seteada por el usuario
#define MODO_7_ENTRA   18                                                       //Entra al menu y permite modificar el offset generado al sensor de la pileta
#define MODO_8_ENTRA   19                                                       //Entra al menu y permite modificar la histeresis entre el punto de sobrecalentamiento de la pileta y el punto en que vuelve a realizar el control
#define MODO_9_ENTRA   20                                                       //Entra al menu y permite modificar la proteccion de sobrecalentamiento en modo manual

