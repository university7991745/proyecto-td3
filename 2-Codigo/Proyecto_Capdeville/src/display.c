#include "display.h"
#include "funciones.h"
#include "timer.h"

extern uint32_t retardo;

void fn_delay(uint32_t demora)
{
	retardo = 0;
	while (retardo < demora);
}

void init_4bits (void)                                                          //Inicializacion a display controlado por 4 bits
{
  uint32_t demora = 50;

  comienzo_init();
  fn_delay(demora);
  cuatro_bits();
  fn_delay(demora);
  cuatro_bits();
  fn_delay(demora);
  activo_display();
  fn_delay(demora);
  return_home();
  fn_delay(demora);
  limpio_display();
  fn_delay(demora);
  selecciono_modo();
  fn_delay(demora);

  limpio_display();
  fn_delay(demora);
}

void cuatro_bits (void)                                                        //Indico que voy a usar el display con 4 bits y uso las cuatro lineas
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void limpio_display (void)                                                      //Limpio pantalla de display y retorna el cursor al inicio
{
	uint32_t demora = 15;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void activo_display (void)                                                      //Prendo el display, coloco el cursor al principio y hago que parpadee el cursor
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void selecciono_modo (void)                                                     //Selecciono el modo de movimiento del cursor del display
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void escribo_letra (uint32_t letra)                                             //Escribo una letra en el display LCD
{
  uint32_t demora = 3;
  uint32_t auxiliar = 0;

  auxiliar = (letra & 0x80) >> 7;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);

  auxiliar = (letra & 0x40) >> 6;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);

  auxiliar = (letra & 0x20) >> 5;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);

  auxiliar = (letra & 0x10) >> 4;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, true);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  auxiliar = (letra & 0x08) >> 3;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);

  auxiliar = (letra & 0x04) >> 2;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);

  auxiliar = (letra & 0x02) >> 1;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);

  auxiliar = (letra & 0x01) >> 0;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, true);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void posicion (uint32_t x, uint32_t y)                                          //En x colocar hasta 16 o 20 dependiendo del display
{
  uint32_t demora = 3;
  uint32_t auxiliar = 0;

  //////////////////////////////////////////////////////////////////////////////
  //                            Determino x
  //////////////////////////////////////////////////////////////////////////////

  x -= 1;                                                                       //Pequeña correccion

  if ((y == 3) || (y == 4)) x += 20;                                            //Escribo sobre linea 3 o 4

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);

  auxiliar = (x & 0x40) >> 6;

  if ((y == 1) || (y == 3))                                                     //Primera y tercer linea
  {
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);

    auxiliar = (x & 0x20) >> 5;
    if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
    else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);

    auxiliar = (x & 0x10) >> 4;
    if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
    else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  }
  else                                                                          //Segunda y cuarta linea
  {
	  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);

    auxiliar = (x & 0x20) >> 5;
    if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
    else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);

    auxiliar = (x & 0x10) >> 4;
    if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
    else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  }

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);


  auxiliar = (x & 0x08) >> 3;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);

  auxiliar = (x & 0x04) >> 2;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);

  auxiliar = (x & 0x02) >> 1;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);

  auxiliar = (x & 0x01) >> 0;
  if (auxiliar) Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, true);
  else          Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

}

void escribo_display (char * palabra, uint32_t x, uint32_t y)                   //Escribo la palabra en el display, y tambien indico la posicion
{
  posicion (x,y);

  while (*palabra != 0)
  {
    escribo_letra(*palabra);
    palabra++;
  }
}

void comienzo_init(void)
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

void return_home (void)                                                         //Retorna al 00
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

}

void reseteo_display (void)                                                      //Prendo el display, coloco el cursor al principio y hago que parpadee el cursor
{
	uint32_t demora = 3;

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, true);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);

  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, true);
  fn_delay(demora);
  Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);
  fn_delay(demora);
}

