#include "funciones.h"
#include "chip.h"
#include "display.h"
#include "memoria.h"
#include <math.h>

//Variables usadas para el ADC
uint16_t dataCOL = 0;
uint16_t dataPIL = 0;

int32_t sensor_colector = 0;
int32_t sensor_pileta = 0;
uint32_t return_menu = 0;

//Variables que uso en las funciones de maquinas de estados
uint32_t menu_programacion = MODO_NULO;

//Variables usadas para los pulsadores
extern uint32_t cant_pas_up;
extern uint32_t cant_pas_down;

//Variables de timer
extern uint32_t flag_cambio;

//Variables de funcionamiento del programa
uint32_t bomba_prendida = 0;                                                    //Flag para saber si tengo la bomba prendida o no
extern uint32_t tiempo_manual;

//Variables de seteo
extern my_configuraciones variables_config;


void init_GPIO(void)
{
	//Inicializacion de salidas

	//Buzzer ==> P2, 13
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, BUZZER);						//Seteo el buzzer como salida
    Chip_GPIO_SetPinState(LPC_GPIO, BUZZER, false);						//Seteo en estado bajo

    //Led Advertencia ==> P2, 5
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_ADVERTENCIA);				//Seteo el led advertencia como salida
    Chip_GPIO_SetPinState(LPC_GPIO, LED_ADVERTENCIA, false);			//Seteo en estado bajo

    //Salida de bomba ==> P0, 28
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, BOMBA);							//Seteo la bomba como salida
    Chip_GPIO_SetPinState(LPC_GPIO, BOMBA, false);						//Seteo en estado bajo


    //Display
    //Salida de display - DISPLAY_RS ==> P0, 9
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_RS);					//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_RS, false);					//Seteo en estado bajo

    //Salida de display - DISPLAY_E ==> P0, 0
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_E);						//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_E, false);					//Seteo en estado bajo

    //Salida de display - DISPLAY_D4 ==> P0, 15
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_D4);					//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D4, false);					//Seteo en estado bajo

    //Salida de display - DISPLAY_D5 ==> P0, 16
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_D5);					//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D5, false);					//Seteo en estado bajo

    //Salida de display - DISPLAY_D6 ==> P0, 23
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_D6);					//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D6, false);					//Seteo en estado bajo

    //Salida de display - DISPLAY_D7 ==> P0, 24
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, DISPLAY_D7);					//Seteo una pata del display
    Chip_GPIO_SetPinState(LPC_GPIO, DISPLAY_D7, false);					//Seteo en estado bajo


    //Incializacion de entradas
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOTON_AUM);						//Seteo pulsador aumento

    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOTON_DISM);						//Seteo pulsador disminucion
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOTON_MODO);						//Seteo pulsador modo
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOTON_PROG);						//Seteo pulsador programacion


    //Inicializo los pines del ADC
	Chip_IOCON_PinMux(LPC_IOCON,PIN_SENSOR_COL,IOCON_MODE_INACT,IOCON_FUNC2);		//Channel 7 - Pin 2
	Chip_IOCON_PinMux(LPC_IOCON,PIN_SENSOR_PIL,IOCON_MODE_INACT,IOCON_FUNC2);		//Channel 6 - Pin 3
}

void pantalla_bienvenida(void)
{
	escribo_display("      AQUARIAL      ", 1, 1);

	escribo_display("PIL:        SET:    ", 1, 3);
	escribo_display("COL:                ", 1, 4);

  muestreo_temperatura(17, 3 , variables_config.temp_pileta);
  muestreo_bomba(15, 4, variables_config.estado_bomba);
}

void muestreo_temperatura(uint32_t punto_x, uint32_t punto_y, uint32_t dato)
{
  uint32_t variable = 0;

  posicion(punto_x, punto_y);
  if (dato / 100 == 0)                                                          //Me fijo en la centena
  {
    escribo_letra('0');

    variable = (dato % 100) / 10 + '0';                                         //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    if (menu_programacion == MODO_4_ENTRA)
    {
      escribo_letra(' ');
      escribo_letra(' ');
    }
    else
    {
      escribo_letra('.');                                                         //Escribo el punto de separacion

      variable = dato % 10 + '0';                                                 //Me quedo con la unidad y lo convierto en ASCII
      escribo_letra(variable);
    }

  }
  else
  {
    variable = dato / 100 + '0';                                                //Me quedo con la centena y lo convierto en ASCII
    escribo_letra(variable);

    variable = (dato % 100) / 10 + '0';                                         //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    if (menu_programacion == MODO_4_ENTRA)
    {
      escribo_letra(' ');
      escribo_letra(' ');
    }
    else
    {
      escribo_letra('.');                                                         //Escribo el punto de separacion

      variable = dato % 10 + '0';                                                 //Me quedo con la unidad y lo convierto en ASCII
      escribo_letra(variable);
    }
  }
}

void muestreo_bomba(uint32_t punto_x, uint32_t punto_y, uint32_t dato)
{
  posicion(punto_x-1, punto_y);

  switch(dato)
  {
    case BOMBA_AUT:
      escribo_display(" AUT", punto_x-1, punto_y);
    break;

    case BOMBA_MAN:
      escribo_display(" MAN", punto_x-1, punto_y);
    break;

    case BOMBA_OFF:
      escribo_display(" OFF", punto_x-1, punto_y);
    break;

    default:
      dato = BOMBA_AUT;
    break;
  }
}

void muestreo_offset(uint32_t punto_x, uint32_t punto_y, int32_t dato)
{
  int32_t variable = 0;

  posicion(punto_x, punto_y);

  if (dato == 0)
  {
    escribo_letra(' ');
    escribo_letra('0');
    escribo_letra('0');
    escribo_letra('.');
    escribo_letra('0');
    return;
  }

  if (dato >= 0)
  {
    escribo_letra(' ');
  }
  else
  {
    escribo_letra('-');
    dato = dato * (-1);
  }

  if (dato / 100 == 0)                                                          //Me fijo en la centena
  {
    escribo_letra('0');

    variable = (dato % 100) / 10 + '0';                                         //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra('.');                                                         //Escribo el punto de separacion

    variable = dato % 10 + '0';                                                 //Me quedo con la unidad y lo convierto en ASCII
    escribo_letra(variable);
  }
  else
  {
    variable = dato / 100 + '0';                                                //Me quedo con la centena y lo convierto en ASCII
    escribo_letra(variable);

    variable = (dato % 100) / 10 + '0';                                         //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra('.');                                                         //Escribo el punto de separacion

    variable = dato % 10 + '0';                                                 //Me quedo con la unidad y lo convierto en ASCII
    escribo_letra(variable);
  }
}

void muestreo_proteccion(uint32_t punto_x, uint32_t punto_y, uint32_t dato)
{
  if (dato == DESHABILITADO) escribo_display("Deshabilitado   ", punto_x, punto_y);
  else                       escribo_display("Habilitado      ", punto_x, punto_y);
}

void vuelta_menu(void)
{
  menu_programacion = MODO_NULO;

  return_menu = 1;
}

uint32_t subo_teclado(uint32_t variable)
{
    if (cant_pas_up < 5) variable++;
    if ((cant_pas_up >= 5) && (cant_pas_up < 10)) variable += 2;
    if ((cant_pas_up >= 10) && (cant_pas_up < 20)) variable += 5;
    if ((cant_pas_up >= 20) && (cant_pas_up < 30)) variable += 10;
    if (cant_pas_up >= 30) variable += 50;

    return variable;
}

uint32_t bajo_teclado(uint32_t variable)
{
  int32_t espejo = variable;

    if (cant_pas_down < 5) espejo--;
    if ((cant_pas_down >= 5) && (cant_pas_down < 10)) espejo -= 2;
    if ((cant_pas_down >= 10) && (cant_pas_down < 20)) espejo -= 5;
    if ((cant_pas_down >= 20) && (cant_pas_down < 30)) espejo -= 10;
    if (cant_pas_down >= 30) espejo -= 50;

    if (espejo < 0) espejo = 0;

    return espejo;
}

int32_t conversion(uint32_t sensor, uint32_t tipo, uint32_t punto_x, uint32_t punto_y)
{
  uint32_t variable = 0;
  int32_t numero_final = 0;
  double calculos = 0;
  static int32_t promedio_pileta[50];
  static int32_t promedio_colector[50];
  static uint32_t contador_pileta = 0, contador_colector = 0;

  calculos = (double) (1000000 * sensor / (4096 - sensor));
  calculos = calculos / 1000000;

  calculos = 1 / (1 / 298.15 + log(calculos) / 3435) - 273.15;

  //En este punto, en calculos tenemos una precision de 0.00X de lo que es el calculo verdadero (diferencias por el 1000000 multiplicando y dividiendo)

  numero_final = (int32_t) (calculos * 10);

/* La ecuacion que voy a usar para saber los valores de temperatura en funcion del NTC es:

  1     1      1     (R )
  -  =  --  +  - * ln(--)               FORMULA A
  T     To     B     (R0)

  Donde:
  T  = temperatura que quiero medir             (en Kelvin)
  To = temperatura de referencia ==>     298.15 (en Kelvin)
  B  = coeficiente asociado al NTC ==>   3435   (en Kelvin)
  R  = resistencia medida en el NTC ==>  DMA    (en Ohm)
  Ro = resistencia parametro del NTC ==> 10K    (en Ohm)

Tengo que adecuar esta formula para lo que yo utilizo que son las lecturas del ADC. Por lo que primero realizo la formula del divisor resistivo entre R y el Rntc, quedando...

         VDD * R
  Vntc = --------                       FORMULA B
         R + Rntc

  Donde:
  Vntc = tension del NTC                 (en Volt)
  VDD  = tension de alimentacion ==> 3.3 (en Volt)
  R    = resistencia de pull-up  ==> 10K (en Ohm)
  Rntc = resistencia del NTC             (en Ohm)

Despejando la R, me queda...

      Vntc * Rntc
  R = -----------                       FORMULA C
      VDD - Vntc

Y meto la formula C dentro de la A para obtener la temperatura en funcion de las tensiones del circuito (recordar que RA = Ro = 10K, por lo que se simplifican)...

  1   1    1      (   Vntc   )
  - = -- + - * ln (----------)          FORMULA D
  T   To   B      (VDD - Vntc)

Ahora agrego otra formula mas, que es la que relacion la lectura del ADC con las tensiones del circuito...

            Vntc
  LECTURA = ---- * 4096                 FORMULA E
            Vdd

Despejo la Vntc...

         LECTURA * Vdd
  Vntc = -------------                  FORMULA F
             4096

Y la meto dentro de la formula D...

  1   1    1      (   LECTURA    )
  - = -- + - * ln (--------------)      FORMULA G
  T   To   B      (4096 - LECTURA)

  Donde:
  T       = temperatura que quiero medir             (en Kelvin)
  To      = temperatura de referencia ==>     298.15 (en Kelvin)
  B       = coeficiente asociado al NTC ==>   3435   (en Kelvin)
  LECTURA = datos del DMA                            (sin unidad)
  4096    = viene dado por el ADC que uso            (sin unidad)

*/

//En este punto, en la variable numero_final tengo lo medido.
//Ahora lo meto en un vector de 50 para que sea un promedio lo que se visualice

  if (tipo == SENSOR_PILETA)
  {
    promedio_pileta[contador_pileta] = numero_final;

    contador_pileta++;
    if (contador_pileta > 49) contador_pileta = 0;

    for (int i = 0; i < 50; i++)
    {
      numero_final += promedio_pileta[i];
    }
  }

  if (tipo == SENSOR_COLECTOR)
  {
    promedio_colector[contador_colector] = numero_final;

    contador_colector++;
    if (contador_colector > 49) contador_colector = 0;

    for (int i = 0; i < 50; i++)
    {
      numero_final += promedio_colector[i];
    }
  }

  numero_final = numero_final / 50;

//Hasta este punto tengo la variable con el promediador, sabiendo si es mayor/menor a 0, lista para mostrar

//Genero esta discriminacion para poder sumar el offset como correccion del usuario
  if (tipo == SENSOR_PILETA)
  {
    numero_final += variables_config.offset_pileta;
  }

  posicion(punto_x, punto_y);

//Caso de que la cuenta sea negativa
  if (numero_final < 0)
  {
//Caso de que la cuenta de menos de -10.0 grados
    if (numero_final < (-100))
    {
      numero_final = -100;
    }

    //Escribo cada un segundo
    if (flag_cambio == 1)
    {
      escribo_letra('-');

      numero_final *= (-1);

      if (numero_final / 100 == 0)                                                  //Me fijo en la centena
      {
        escribo_letra('0');

        variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
        escribo_letra(variable);

        escribo_letra('.');                                                         //Escribo el punto de separacion

        variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
        escribo_letra(variable);

        escribo_letra(' ');
      }
      else
      {
        variable = numero_final / 100 + '0';                                        //Me quedo con la centena y lo convierto en ASCII
        escribo_letra(variable);

        variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
        escribo_letra(variable);

        escribo_letra('.');                                                         //Escribo el punto de separacion

        variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
        escribo_letra(variable);

        escribo_letra(' ');
      }

      numero_final *= (-1);


      if (tipo == SENSOR_COLECTOR) flag_cambio = 0;
    }
    return numero_final;
  }


//Caso de que la cuenta de mas de 99.9 grados
  if (numero_final > 999)
  {
    escribo_letra('E');
    escribo_letra('R');
    escribo_letra('R');
    escribo_letra('O');
    escribo_letra('R');

    return numero_final;
  }

  if (flag_cambio == 1)
  {
    if (numero_final / 100 == 0)                                                  //Me fijo en la centena
    {
      escribo_letra('0');

      variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
      escribo_letra(variable);

      escribo_letra('.');                                                         //Escribo el punto de separacion

      variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
      escribo_letra(variable);

      escribo_letra(' ');
    }
    else
    {
      variable = numero_final / 100 + '0';                                        //Me quedo con la centena y lo convierto en ASCII
      escribo_letra(variable);

      variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
      escribo_letra(variable);

      escribo_letra('.');                                                         //Escribo el punto de separacion

      variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
      escribo_letra(variable);

      escribo_letra(' ');
    }

    if (tipo == SENSOR_COLECTOR) flag_cambio = 0;
  }

  return numero_final;
}

int32_t fn_adquisicion_datos(uint16_t data)
{
	int32_t calculo = (int32_t) data;

	calculo = calculo * 1099 / 2900;					//Acoto el rango a lo deseado (0°C a 109,9°C)
	if (calculo > 1099) calculo = 1099;					//Limite superior en 109,9°C

	calculo = calculo - 100;							//Limite inferior en -10.0°C y superior en 99.9°C

	return calculo;
}

void muestreo_sensores(uint32_t punto_x, uint32_t punto_y, int32_t numero_final)
{
  uint32_t variable = 0;

  posicion(punto_x, punto_y);

//Caso de que la cuenta sea negativa
  if (numero_final < 0)
  {
//Caso de que la cuenta de menos de -10.0 grados
	if (numero_final < (-100))
	{
	  numero_final = -100;
	}

	//Escribo cada un segundo
	  escribo_letra('-');

	  numero_final *= (-1);

	  if (numero_final / 100 == 0)                                                  //Me fijo en la centena
	  {
		escribo_letra('0');

		variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
		escribo_letra(variable);

		escribo_letra('.');                                                         //Escribo el punto de separacion

		variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
		escribo_letra(variable);

		escribo_letra(' ');
	  }
	  else
	  {
		variable = numero_final / 100 + '0';                                        //Me quedo con la centena y lo convierto en ASCII
		escribo_letra(variable);

		variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
		escribo_letra(variable);

		escribo_letra('.');                                                         //Escribo el punto de separacion

		variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
		escribo_letra(variable);

		escribo_letra(' ');
	  }
	return;
  }

  if (numero_final > 999)
  {
    escribo_letra('E');
    escribo_letra('R');
    escribo_letra('R');
    escribo_letra('O');
    escribo_letra('R');

    return;
  }

  if (numero_final / 100 == 0)                                                  //Me fijo en la centena
  {
    escribo_letra('0');

    variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra('.');                                                         //Escribo el punto de separacion

    variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra(' ');
  }
  else
  {
    variable = numero_final / 100 + '0';                                        //Me quedo con la centena y lo convierto en ASCII
    escribo_letra(variable);

    variable = (numero_final % 100) / 10 + '0';                                 //Me quedo con la decena y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra('.');                                                         //Escribo el punto de separacion

    variable = numero_final % 10 + '0';                                         //Me quedo con la unidad y lo convierto en ASCII
    escribo_letra(variable);

    escribo_letra(' ');
  }
}

void fn_accionamiento_manual(void)
{
//En caso de ser bomba manual, debo entrar aca
	if (variables_config.estado_bomba == BOMBA_MAN)
	{
	  if (tiempo_manual < 21600)                                                //21600 segundos ==> 360 minutos ==> 6 horas
	  {
		Chip_GPIO_SetPinState(LPC_GPIO, BOMBA, true);
		bomba_prendida = 1;
	  }
	  else
	  {
		Chip_GPIO_SetPinState(LPC_GPIO, BOMBA, false);
		bomba_prendida = 0;
	  }
	}
}

void accionamiento_bomba(uint32_t estado)
{
  if (estado == HABILITADO)   Chip_GPIO_SetPinState(LPC_GPIO, BOMBA, true);
  else                        Chip_GPIO_SetPinState(LPC_GPIO, BOMBA, false);
}

void accionamiento_led_advertencia(uint32_t estado)
{
  if (estado == HABILITADO) Chip_GPIO_SetPinState(LPC_GPIO, LED_ADVERTENCIA, true);
  else                      Chip_GPIO_SetPinState(LPC_GPIO, LED_ADVERTENCIA, false);
}

void fn_accionamiento_autom(void)
{
	static uint32_t histeresis_anticong = 0;                                               //Flag que uso para marcar si entro por anticongelamiento o no

    if (((((((sensor_colector - sensor_pileta > variables_config.dif_temp_on) && (sensor_colector > sensor_pileta) && (sensor_pileta < variables_config.temp_pileta))
           && (sensor_pileta < (variables_config.temp_pileta - variables_config.histeresis))) && (sensor_colector > sensor_pileta) && (sensor_colector < variables_config.sobrecal)) || (sensor_colector < (int32_t) variables_config.anticong)) && (variables_config.estado_bomba == BOMBA_AUT))
        || ((histeresis_anticong == 1) && (sensor_colector < (int32_t) (variables_config.anticong + ANTICONG_HISTERESIS))))
    {
      accionamiento_bomba(HABILITADO);
      bomba_prendida = 1;

      if (sensor_colector < (int32_t) variables_config.anticong) histeresis_anticong = 1;                                                //Indico que se prendio por el anticongelamiento

//Prendo el led de advertencia
      if ((((sensor_colector < variables_config.anticong) || (sensor_colector > variables_config.sobrecal)) && (menu_programacion == MODO_NULO)) || ((histeresis_anticong == 1) && (sensor_colector < (int32_t) (variables_config.anticong + ANTICONG_HISTERESIS))))
      {
        accionamiento_led_advertencia(HABILITADO);

        if ((sensor_colector < (int32_t) variables_config.anticong) || (histeresis_anticong == 1))          escribo_display("A", 13, 4);
        else                                                                               escribo_display("S", 13, 4);
      }
      else
      {
        accionamiento_led_advertencia(DESHABILITADO);
        if (menu_programacion == MODO_NULO) escribo_display(" ", 13, 4);
      }
    }
    else
    {
//Corresponde a la funcion 2 ==> apagado de la bomba
//Corresponde a la funcion 4 ==> temperatura de sobrecalentamiento
      if ((((((((sensor_colector - sensor_pileta < variables_config.dif_temp_off) && (sensor_colector > sensor_pileta)) || (sensor_pileta > variables_config.temp_pileta))
             && (bomba_prendida == 1)) || (sensor_colector > variables_config.sobrecal)) || (sensor_pileta > variables_config.temp_pileta)) || (sensor_colector < sensor_pileta)) || (variables_config.estado_bomba == BOMBA_OFF))
      {
        if ((variables_config.estado_bomba == BOMBA_OFF) || (variables_config.estado_bomba == BOMBA_AUT))
        {
          accionamiento_bomba(DESHABILITADO);
          bomba_prendida = 0;

          if (sensor_colector > (int32_t) (variables_config.anticong + ANTICONG_HISTERESIS)) histeresis_anticong = 0;                    //Apago el flag luego de la histeresis generada
        }

//Prendo el led de advertencia
        if (((sensor_colector < variables_config.anticong) || (sensor_colector > variables_config.sobrecal)) && (menu_programacion == MODO_NULO))
        {
          accionamiento_led_advertencia(HABILITADO);

          if (sensor_colector < (int32_t) variables_config.anticong)          escribo_display("A", 13, 4);
          else                                               escribo_display("S", 13, 4);
        }
        else
        {
          accionamiento_led_advertencia(DESHABILITADO);
          if (menu_programacion == MODO_NULO) escribo_display(" ", 13, 4);
        }
      }
    }
}

void muestreo_menu(void)
{
	if (return_menu == 1)
	{
		escribo_display("      AQUARIAL      ", 1, 1);

		escribo_display("PIL:        SET:    ", 1, 3);
		muestreo_sensores(5, 3, sensor_pileta);
		muestreo_temperatura(17, 3, variables_config.temp_pileta);
		escribo_display("COL:                ", 1, 4);
		muestreo_sensores(5, 4, sensor_colector);
		muestreo_bomba(15, 4, variables_config.estado_bomba);

		return_menu = 0;
	}
	else
	{
	    if (menu_programacion == MODO_NULO)
	    {
			muestreo_sensores(5, 3, sensor_pileta);
			muestreo_sensores(5, 4, sensor_colector);
	    }
	}
}

void tomo_sensor_colector(void)
{
	App_Polling_Test(7);				//dataCOL
	sensor_colector = fn_adquisicion_datos(dataCOL);
}

void tomo_sensor_pileta(void)
{
	App_Polling_Test(6);				//dataPIL
	sensor_pileta = fn_adquisicion_datos(dataPIL);
}
