#include "funciones.h"
#include "timer.h"
#include "Tareas/tarea_pulsadores.h"

uint32_t retardo = 0;
uint32_t segundos = 0;                                                          //Variables de timer
uint32_t tiempo_manual = 0;                                                     //Tiempo que transcurre cuando prendo la bomba de forma manual
uint32_t flag_cambio = 0;                                                       //Flag que permite cambiar lo que veo en el display

void init_timer1(void)
{
	/* Habilita el timer elegido */
	Chip_TIMER_Init(LPC_TIMER0);

    /* Setea el timer en modo Match e Interrupción */
	Chip_TIMER_Reset(LPC_TIMER0);
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, 1);

	//Seteo la frecuencia a la que tiene que ir el eje x en la pantalla del osciloscopio
	Chip_TIMER_SetMatch(LPC_TIMER0, 1, 25000);

	/* Resetea en el match */
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, 1);
	Chip_TIMER_StopOnMatchDisable(LPC_TIMER0, 1);

	/* Habilita la interrupción del timer */
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	NVIC_EnableIRQ(TIMER0_IRQn);

	/* Arranca el timer */
	Chip_TIMER_Enable(LPC_TIMER0);
}

void TIMER0_IRQHandler(void)
{
	static uint32_t contador = 0;

	if (Chip_TIMER_MatchPending(LPC_TIMER0, 1))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER0, 1);

//Funciones de teclado
		Teclado_aum();                                                                //Funcion para leer los pulsadores
		Teclado_dism();                                                               //Funcion para leer los pulsadores
		Teclado_prog();                                                               //Funcion para leer los pulsadores
		Teclado_modo();                                                               //Funcion para leer los pulsadores

		contador++;
		retardo++;

		if (contador > 1000)
		{
			contador = 0;
			segundos++;

			tiempo_manual++;

			flag_cambio++;
		}
	}
}

