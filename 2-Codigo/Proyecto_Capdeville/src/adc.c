#include "funciones.h"
#include "adc.h"

extern uint16_t dataCOL;
extern uint16_t dataPIL;


void App_Polling_Test(uint32_t canal)
{
	static ADC_CLOCK_SETUP_T ADCSetup;

	if (canal == 7)
	{
		Chip_ADC_Init(LPC_ADC, &ADCSetup);
		Chip_ADC_EnableChannel(LPC_ADC, ADC_CH6, DISABLE);
		Chip_ADC_EnableChannel(LPC_ADC, ADC_CH7, ENABLE);

		Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);														//Selecciono modo burst o no

		while (Chip_ADC_ReadStatus(LPC_ADC, ADC_CH7, ADC_DR_DONE_STAT) != SET);						//Esperando a que termine la conversion

		Chip_ADC_ReadValue(LPC_ADC, ADC_CH7, &dataCOL);												//Leo el valor del ADC
		Chip_ADC_SetBurstCmd(LPC_ADC, DISABLE);														//Selecciono modo burst o no
	}

	if (canal == 6)
	{
		Chip_ADC_Init(LPC_ADC, &ADCSetup);
		Chip_ADC_EnableChannel(LPC_ADC, ADC_CH7, DISABLE);
		Chip_ADC_EnableChannel(LPC_ADC, ADC_CH6, ENABLE);

		Chip_ADC_SetBurstCmd(LPC_ADC, ENABLE);														//Selecciono modo burst o no

		while (Chip_ADC_ReadStatus(LPC_ADC, ADC_CH6, ADC_DR_DONE_STAT) != SET);						//Esperando a que termine la conversion

		Chip_ADC_ReadValue(LPC_ADC, ADC_CH6, &dataPIL);												//Leo el valor del ADC
		Chip_ADC_SetBurstCmd(LPC_ADC, DISABLE);														//Selecciono modo burst o no
	}
}
