#include "memoria.h"
#include "funciones.h"


//Variables de seteo
uint32_t vector_datos[10];														//Variables de configuracion del programa
my_configuraciones variables_config = {0, 300, BOMBA_AUT, 50, 25, 50, 850, 850, 20, DESHABILITADO};


void I2C1_IRQHandler(void)
{
	Chip_I2C_MasterStateHandler(I2C1);
}


void init_i2c(void){

	/* Configuración de los pines. La EEPROM está conectada al puerto I2C1. */
	Chip_IOCON_PinMux(LPC_IOCON, SDA1_PORT, SDA1_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, SCL1_PORT, SCL1_PIN, IOCON_MODE_INACT, IOCON_FUNC3);

	/* Importante habilitar OPEN DRAIN */
	Chip_IOCON_EnableOD(LPC_IOCON, SDA1_PORT, SDA1_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON, SCL1_PORT, SCL1_PIN);

	/* Habilita el módulo */
	Chip_I2C_Init(I2C1);

	/* Establece el clock */
	Chip_I2C_SetClockRate(I2C1, 100000);

	/* Handler de I2C Modo Master */
	Chip_I2C_SetMasterEventHandler(I2C1, Chip_I2C_EventHandler);

	/* Habilita la interrupción */
	NVIC_EnableIRQ(I2C1_IRQn);
}

void fn_escribo_EEPROM(uint32_t posicion, uint32_t buffer)
{
	unsigned char Datos_Tx [] = { ((W_ADDRESS + posicion * 32) & 0xFF00) >> 8, ((W_ADDRESS + posicion * 32) & 0x00FF), 0, 0, 0, 0};

	//Espejo esto para escribirlo en memoria y sacarlo bien en la lectura
	Datos_Tx[2] = buffer & 0x000000FF;
	Datos_Tx[3] = (buffer & 0x0000FF00) >> 8;
	Datos_Tx[4] = (buffer & 0x00FF0000) >> 16;
	Datos_Tx[5] = (buffer & 0xFF000000) >> 24;

	//Mucho cuidado que escribe de menor a mayor ===> en la memoria queda, por ejemplo:
	//si char Datos_Tx [] = { (W_ADDRESS & 0xFF00) >> 8 , W_ADDRESS & 0x00FF , 10, 11, 12, 13};
	//en la memoria queda escrito 0x0d0c0b0a

	Chip_I2C_MasterSend (I2C1, SLAVE_ADDRESS, Datos_Tx, 6);
	fn_delay(50);
}

void fn_leo_EEPROM(uint32_t posicion, uint32_t * buffer)
{
//	Buffer de lectura
	unsigned char Datos_Tx [] = { ((W_ADDRESS + posicion * 32) & 0xFF00) >> 8 , ((W_ADDRESS + posicion * 32) & 0x00FF) };

	   /* Se posiciona donde se guardaron los datos (sólo se envían los datos de posición) */
	   Chip_I2C_MasterSend (I2C1, SLAVE_ADDRESS, Datos_Tx, 2);

	   /* Se leen los datos */
	   Chip_I2C_MasterRead (I2C1, SLAVE_ADDRESS, (void *) buffer, 4);
}

void lectura_memoria_EEPROM(void)
{
	//Lectura de todas las configuraciones del calefactor solar
	fn_leo_EEPROM(MEM_TEMP_PILETA,    vector_datos);
	variables_config.temp_pileta       = (int32_t)  vector_datos[0];

	fn_leo_EEPROM(MEM_DIF_TEMP_ON,    vector_datos);
	variables_config.dif_temp_on       = (int32_t)  vector_datos[0];

	fn_leo_EEPROM(MEM_DIF_TEMP_OFF,   vector_datos);
	variables_config.dif_temp_off      = (int32_t)  vector_datos[0];

	fn_leo_EEPROM(MEM_ANTICONG,       vector_datos);
	variables_config.anticong          = (uint32_t) vector_datos[0];

	fn_leo_EEPROM(MEM_SOBRECAL,       vector_datos);
	variables_config.sobrecal          = (uint32_t) vector_datos[0];

	fn_leo_EEPROM(MEM_TEMP_MAX_PILETA,vector_datos);
	variables_config.temp_max_pileta   = (uint32_t) vector_datos[0];

	fn_leo_EEPROM(MEM_OFFSET_PILETA,  vector_datos);
	variables_config.offset_pileta     = (int32_t)  vector_datos[0];

	fn_leo_EEPROM(MEM_HISTERESIS,     vector_datos);
	variables_config.histeresis        = (uint32_t) vector_datos[0];

	fn_leo_EEPROM(MEM_PROTECCION_MAN, vector_datos);
	variables_config.proteccion_manual = (uint32_t) vector_datos[0];

	fn_leo_EEPROM(MEM_ESTADO_BOMBA,   vector_datos);
	variables_config.estado_bomba      = (uint32_t) vector_datos[0];
}
