#include <Tareas/tarea_pulsadores.h>
#include "chip.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "funciones.h"
#include "timer.h"
#include "display.h"
#include "adc.h"
#include "memoria.h"

//Handler usados
xSemaphoreHandle sTask_Manager;

//Task_Pulsadores: lectura de pulsadores + maquina de estado asociadas
static void task_Pulsadores(void *pvParameters)
{
	xSemaphoreTake(sTask_Manager, portMAX_DELAY);

	while(1)
	{
		//Lectura de pulsadores y decido que pasa en cada caso
		fn_prog_modo();

		//Maquina de estados asociada al pulsador de menu_visual
		maq_est_menu_visual();

		//Maquina de estados asociada al pulsador de menu_programacion
		maq_est_menu_prog();

		//Demora para que se vaya de la tarea y que pueda tomar otras
		vTaskDelay(TEMP_DELAY_TASK/portTICK_RATE_MS);

		xSemaphoreGive(sTask_Manager);
	}
}

//Task_Display_LCD: escritura del display LCD + maquinas de estado asociadas
static void task_Display_LCD(void *pvParameters)
{
	xSemaphoreTake(sTask_Manager, portMAX_DELAY);

	while(1)
	{
		//Muestreo de la pantalla del menu
		muestreo_menu();

		//Demora para que se vaya de la tarea y que pueda tomar otras
		vTaskDelay(TEMP_DELAY_TASK/portTICK_RATE_MS);

		xSemaphoreGive(sTask_Manager);
	}
}

//Task ADC: manejo del ADC y sus funciones
static void task_ADC_COL(void *pvParameters)
{
	xSemaphoreTake(sTask_Manager, portMAX_DELAY);

	while(1)
	{
		//Tomo el valor del sensor del colector
		tomo_sensor_colector();

		//Demora para que se vaya de la tarea y que pueda tomar otras
		vTaskDelay(TEMP_DELAY_TASK/portTICK_RATE_MS);

		xSemaphoreGive(sTask_Manager);
	}
}

//Task ADC: manejo del ADC y sus funciones
static void task_ADC_PIL(void *pvParameters)
{
	xSemaphoreTake(sTask_Manager, portMAX_DELAY);

	while(1)
	{
		//Tomo el valor del sensor de pileta
		tomo_sensor_pileta();

		//Demora para que se vaya de la tarea y que pueda tomar otras
		vTaskDelay(TEMP_DELAY_TASK/portTICK_RATE_MS);

		xSemaphoreGive(sTask_Manager);
	}
}

//Task Salidas: manejo del led, buzzer y rele de bomba
static void task_Salidas(void *pvParameters)
{
	xSemaphoreTake(sTask_Manager, portMAX_DELAY);

	while(1)
	{
		//Accionamiento automatico
		fn_accionamiento_autom();

		//Accionamiento manual
		fn_accionamiento_manual();

		//Demora para que se vaya de la tarea y que pueda tomar otras
		vTaskDelay(TEMP_DELAY_TASK/portTICK_RATE_MS);

		xSemaphoreGive(sTask_Manager);
	}
}

int main(void)
{
	SystemCoreClockUpdate();

	init_GPIO();										//Inicializo puertos de entrada y salida
	init_timer1();										//Inicializo el timer1
	init_i2c();											//Inicializo la memoria EEPROM

	lectura_memoria_EEPROM();							//Traigo de la memoria todos los datos que uso
	fn_delay(2000);
	init_4bits();										//Inicializo el display LCD
	limpio_display();
	pantalla_bienvenida();								//Pantalla de arranque del display

//Creación de los semáforos
	sTask_Manager = xSemaphoreCreateMutex();
	while (!sTask_Manager);

//Condición inicial: los semaphoros arrancan liberados

//Creacion de tareas	(mayor numero ==> mayor prioridad)
	xTaskCreate(task_Pulsadores, (const signed char * const) "Tarea PULSADORES", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 2UL), (xTaskHandle *) NULL);
	xTaskCreate(task_ADC_COL, (const signed char * const) "Tarea ADC_COL", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 4UL), (xTaskHandle *) NULL);
	xTaskCreate(task_ADC_PIL, (const signed char * const) "Tarea ADC_PIL", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 4UL), (xTaskHandle *) NULL);
	xTaskCreate(task_Salidas, (const signed char * const) "Tarea Salidas", configMINIMAL_STACK_SIZE,
			NULL, (tskIDLE_PRIORITY + 3UL), (xTaskHandle *) NULL);
    xTaskCreate(task_Display_LCD, (const signed char * const) "Tarea DISPLAY LCD", configMINIMAL_STACK_SIZE,
    		NULL, (tskIDLE_PRIORITY + 5UL), (xTaskHandle *) NULL);


//	Arranca el Scheduler
	vTaskStartScheduler();

// 	Nunca debería llegar acá

	while(1)
	{
/*
// Por si me mando alguna macana, con las memorias, esto pone los valores por defecto
        fn_escribo_EEPROM(0, 300);
        fn_escribo_EEPROM(1, 50);
        fn_escribo_EEPROM(2, 25);
        fn_escribo_EEPROM(3, 50);
        fn_escribo_EEPROM(4, 850);
        fn_escribo_EEPROM(5, 850);
        fn_escribo_EEPROM(6, 0);
        fn_escribo_EEPROM(7, 20);
        fn_escribo_EEPROM(8, 0);
        fn_escribo_EEPROM(9, 1);
*/
	}

    return 0;
}

