#include <Tareas/tarea_pulsadores.h>
#include "memoria.h"
#include "funciones.h"

extern int32_t sensor_colector;
extern int32_t sensor_pileta;


//Variables que uso en las funciones
uint32_t tecla = 0;                                                             //Variable para leer botones

//Variables de timer
extern uint32_t segundos;

//Variables que uso en las funciones de maquinas de estados
uint32_t cant_pas_up = 0;
uint32_t cant_pas_down = 0;

//Variables usadas para los pulsadores
uint32_t pulsadores = 0;                                                        //Lectura de pulsador

//Variables usadas para maquinas de estados
uint32_t menu_visual = MENU_NULO;
extern uint32_t menu_programacion;

//Variables de funcionamiento del programa
extern my_configuraciones variables_config;
extern uint32_t tiempo_manual;



void Teclado_aum()
{
  static uint32_t codigo_anterior = 0;
  static uint32_t estado = 0;
  static uint32_t codigo_actual = 0;

  codigo_actual = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_AUM);

  if (codigo_actual == 1)
  {
    estado = 0;
    codigo_anterior = 1;
    cant_pas_up = 0;
    return;
  }

  if (estado == 0)
  {
    estado++;
    codigo_anterior = codigo_actual;
    return;
  }

  if (estado == 8)                                                              //Cantidad de rebotes permitidos
  {
    estado++;
    tecla = PULS_AUM;
    return;
  }

  if (estado > 500)
  {
    estado = 0;
    codigo_anterior = 1;
    cant_pas_up++;
    return;
  }

  if (codigo_actual == codigo_anterior) estado++;
  else                                  estado = 0;
  return;
}

void Teclado_dism()
{
  static uint32_t codigo_anterior = 0;
  static uint32_t estado = 0;
  static uint32_t codigo_actual = 0;

  codigo_actual = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_DISM);

  if (codigo_actual == 1)
  {
    estado = 0;
    codigo_anterior = 1;
    cant_pas_down = 0;
    return;
  }

  if (estado == 0)
  {
    estado++;
    codigo_anterior = codigo_actual;
    return;
  }

  if (estado == 8)                                                              //Cantidad de rebotes permitidos
  {
    estado++;
    tecla = PULS_DIS;
    return;
  }

  if (estado > 500)
  {
    estado = 0;
    codigo_anterior = 1;
    cant_pas_down++;
    return;
  }

  if (codigo_actual == codigo_anterior) estado++;
  else                                  estado = 0;
  return;
}

void Teclado_prog()
{
  static uint32_t codigo_anterior = 0;
  static uint32_t estado = 0;
  static uint32_t codigo_actual = 0;

  codigo_actual = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_PROG);

  if (codigo_actual == 1)
  {
    estado = 0;
    codigo_anterior = 1;
    return;
  }

  if (estado == 0)
  {
    estado++;
    codigo_anterior = codigo_actual;
    return;
  }

  if (estado == 8)                                                              //Cantidad de rebotes permitidos
  {
    estado++;
    tecla = PULS_PROG;
    return;
  }

  if (estado > 8)    return;

  if (codigo_actual == codigo_anterior) estado++;
  else                                  estado = 0;
  return;
}

void Teclado_modo()
{
  static uint32_t codigo_anterior = 0;
  static uint32_t estado = 0;
  static uint32_t codigo_actual = 0;

  codigo_actual = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_MODO);

  if (codigo_actual == 1)
  {
    estado = 0;
    codigo_anterior = 1;
    return;
  }

  if (estado == 0)
  {
    estado++;
    codigo_anterior = codigo_actual;
    return;
  }

  if (estado == 8)                                                              //Cantidad de rebotes permitidos
  {
    estado++;
    tecla = PULS_MODO;
    return;
  }

  if (estado > 8)    return;

  if (codigo_actual == codigo_anterior) estado++;
  else                                  estado = 0;
  return;
}

uint32_t Tecla()
{
  uint32_t Tecla;

  Tecla = tecla;
  tecla = 0;

  return Tecla;
}

uint32_t puls_aum(void)
{
  uint32_t pulsador = 0;

  pulsador = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_AUM);

  return pulsador;
}

uint32_t puls_dism(void)
{
  uint32_t pulsador = 0;

  pulsador = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_DISM);

  return pulsador;
}

uint32_t puls_prog(void)
{
  uint32_t pulsador = 0;

  pulsador = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_PROG);

  return pulsador;
}

uint32_t puls_modo(void)
{
  uint32_t pulsador = 0;

  pulsador = Chip_GPIO_GetPinState(LPC_GPIO, BOTON_MODO);

  return pulsador;
}

void fn_prog_modo (void)
{
	pulsadores = Tecla();

    if (pulsadores == PULS_PROG)
    {
      if (menu_programacion == MODO_NULO)
      {
        switch (menu_visual)
        {
          case MENU_NULO:
            //Cambio de menu
            menu_visual = MENU_TEMP;
          break;

          case MENU_TEMP:
            //Escribo los datos en el display para evitar que se quede en blanco (al cambiar en segundo impar)
            muestreo_temperatura(17, 3, variables_config.temp_pileta);
            fn_escribo_EEPROM(MEM_TEMP_PILETA, variables_config.temp_pileta);

            //Cambio de menu
            menu_visual = MENU_BOMBA;
          break;

          case MENU_BOMBA:
            //Escribo los datos en el display para evitar que se quede en blanco (al cambiar en segundo impar)
            muestreo_bomba(15, 4, variables_config.estado_bomba);
            fn_escribo_EEPROM(MEM_ESTADO_BOMBA, variables_config.estado_bomba);

            //Cambio de menu
            menu_visual = MENU_NULO;
          break;

          default:
            //Cambio de menu
            menu_visual = MENU_NULO;
          break;
        }
      }
    }

    if (pulsadores == PULS_MODO)
    {
      if (menu_visual == MENU_NULO)
      {
        switch(menu_programacion)
        {
          case MODO_NULO:
            menu_programacion = MODO_1;

            escribo_display("=>Dif Temp Bomba ON ", 1, 2);
            escribo_display("  Dif Temp Bomba OFF", 1, 3);
            escribo_display("  Anticongelamiento ", 1, 4);
          break;

          case MODO_1:
            menu_programacion = MODO_1_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Dif Temp Bomba ON   ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.dif_temp_on);
            escribo_display("            ", 9, 4);
          break;

          case MODO_2:
            menu_programacion = MODO_2_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Dif Temp Bomba OFF  ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.dif_temp_off);
            escribo_display("            ", 9, 4);
          break;

          case MODO_3:
            menu_programacion = MODO_3_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Anticongelamiento   ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.anticong);
            escribo_display("            ", 9, 4);
          break;

          case MODO_4:
            menu_programacion = MODO_4_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Sobrecalentamiento  ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.sobrecal);
            escribo_display("            ", 9, 4);
          break;

          case MODO_6:
            menu_programacion = MODO_6_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Temp Max Pileta     ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.temp_max_pileta);
            escribo_display("            ", 9, 4);
          break;

          case MODO_7:
            menu_programacion = MODO_7_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Ajuste Temp Pileta  ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_offset(5, 4, variables_config.offset_pileta);
            escribo_display("            ", 10, 4);
          break;

          case MODO_8:
            menu_programacion = MODO_8_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Prog Diferencia     ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_temperatura(5, 4, variables_config.histeresis);
            escribo_display("            ", 9, 4);
          break;

          case MODO_9:
            menu_programacion = MODO_9_ENTRA;

            escribo_display("                    ", 1, 2);
            escribo_display("Proteccion          ", 1, 3);
            escribo_display("Set:", 1, 4);
            muestreo_proteccion(5, 4, variables_config.proteccion_manual);
          break;

          case MODO_1_ENTRA:
        	fn_escribo_EEPROM(MEM_DIF_TEMP_ON, variables_config.dif_temp_on);
            vuelta_menu();
            break;

          case MODO_2_ENTRA:
          	fn_escribo_EEPROM(MEM_DIF_TEMP_OFF, variables_config.dif_temp_off);
            vuelta_menu();
            break;

          case MODO_3_ENTRA:
            fn_escribo_EEPROM(MEM_ANTICONG, variables_config.anticong);
            vuelta_menu();
            break;

          case MODO_4_ENTRA:
            fn_escribo_EEPROM(MEM_SOBRECAL, variables_config.sobrecal);
            vuelta_menu();
            break;

          case MODO_6_ENTRA:
            fn_escribo_EEPROM(MEM_TEMP_MAX_PILETA, variables_config.temp_max_pileta);
            vuelta_menu();
            break;

          case MODO_7_ENTRA:
            fn_escribo_EEPROM(MEM_OFFSET_PILETA, variables_config.offset_pileta);
            vuelta_menu();
            break;

          case MODO_8_ENTRA:
            fn_escribo_EEPROM(MEM_HISTERESIS, variables_config.histeresis);
            vuelta_menu();
            break;

          case MODO_9_ENTRA:
            fn_escribo_EEPROM(MEM_PROTECCION_MAN, variables_config.proteccion_manual);
            vuelta_menu();
          break;

          case MODO_REGRESAR:                                                   //Es el listado de las funciones que hay
            vuelta_menu();
          break;

          default:
            menu_programacion = MODO_NULO;
          break;
        }
      }
    }
}

void maq_est_menu_visual (void)
{
  switch(menu_visual)
  {
      case MENU_NULO:

      break;

      case MENU_TEMP:
        if (segundos % 2 == 0)  escribo_display("    ", 17, 3);
        else                    muestreo_temperatura(17, 3, variables_config.temp_pileta);

        if (pulsadores == PULS_AUM)
        {
          variables_config.temp_pileta = subo_teclado(variables_config.temp_pileta);                                //Hago una suba logaritmica al mantener apretado el pulsador

          if (variables_config.temp_pileta > variables_config.temp_max_pileta) variables_config.temp_pileta = variables_config.temp_max_pileta;     //Limite superior de la variable
        }

        if (pulsadores == PULS_DIS)
        {
          variables_config.temp_pileta = bajo_teclado(variables_config.temp_pileta);

          if (variables_config.temp_pileta < TEMP_LIM_DOWN) variables_config.temp_pileta = TEMP_LIM_DOWN;         //Limite inferior de la variable
        }
      break;

      case MENU_BOMBA:
        if (segundos % 2 == 0)  escribo_display("   ", 15, 4);
        else                    muestreo_bomba(15, 4, variables_config.estado_bomba);

        if (pulsadores == PULS_AUM)
        {
          variables_config.estado_bomba++;

          if (variables_config.estado_bomba > 3) variables_config.estado_bomba = 1;                               //Limite superior de la variable

          if (variables_config.estado_bomba == BOMBA_MAN) tiempo_manual = 0;                     //Reseteo el contador de segundos
        }

        if (pulsadores == PULS_DIS)
        {
          variables_config.estado_bomba--;

          if (variables_config.estado_bomba < 1) variables_config.estado_bomba = 3;                               //Limite inferior de la variable

          if (variables_config.estado_bomba == BOMBA_MAN) tiempo_manual = 0;
        }
      break;

      default:
        menu_visual = MENU_NULO;
      break;
  }
}

void maq_est_menu_prog(void)
{
  switch (menu_programacion)
  {
    case MODO_NULO:
    	//Tomo los valores de los sensores y los muestro


    	//Si usara los termistores NTC
		//sensor_pileta = conversion(dataPIL, SENSOR_PILETA, 5, 3);
		//sensor_colector = conversion(dataCOL, SENSOR_COLECTOR, 5, 4);
    break;

    case MODO_1:                                                                //Es el listado de las funciones que hay
      /*Muestra:
      "      AQUARIAL      "
      "=>Dif Temp Bomba ON "
      "  Dif Temp Bomba OFF"
      "  Anticongelamiento "
      */
      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_2;

        escribo_display("  Dif Temp Bomba ON ", 1, 2);
        escribo_display("=>Dif Temp Bomba OFF", 1, 3);
        escribo_display("  Anticongelamiento ", 1, 4);
      }

    break;

    case MODO_2:
      /*Muestra:
      "      AQUARIAL      "
      "  Dif Temp Bomba ON "
      "=>Dif Temp Bomba OFF"
      "  Anticongelamiento "
      */
      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_1;

        escribo_display("=>Dif Temp Bomba ON ", 1, 2);
        escribo_display("  Dif Temp Bomba OFF", 1, 3);
        escribo_display("  Anticongelamiento ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_3;

        escribo_display("  Dif Temp Bomba OFF", 1, 2);
        escribo_display("=>Anticongelamiento ", 1, 3);
        escribo_display("  Sobrecalentamiento", 1, 4);
      }
    break;

    case MODO_3:
      /*Muestra:
      "      AQUARIAL      "
      "  Dif Temp Bomba OFF"
      "=>Anticongelamiento "
      "  Sobrecalentamiento"
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_2;

        escribo_display("  Dif Temp Bomba ON ", 1, 2);
        escribo_display("=>Dif Temp Bomba OFF", 1, 3);
        escribo_display("  Anticongelamiento ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_4;

        escribo_display("  Anticongelamiento ", 1, 2);
        escribo_display("=>Sobrecalentamiento", 1, 3);
        escribo_display("  Temp Max Pileta   ", 1, 4);
      }
    break;

    case MODO_4:
      /*Muestra:
      "      AQUARIAL      "
      "  Anticongelamiento "
      "=>Sobrecalentamiento"
      "  Temp Max Pileta   "
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_3;

        escribo_display("  Dif Temp Bomba OFF", 1, 2);
        escribo_display("=>Anticongelamiento ", 1, 3);
        escribo_display("  Sobrecalentamiento", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_6;

        escribo_display("  Sobrecalentamiento", 1, 2);
        escribo_display("=>Temp Max Pileta   ", 1, 3);
        escribo_display("  Ajuste Sensor     ", 1, 4);
      }
    break;

    case MODO_6:
      /*Muestra:
      "      AQUARIAL      "
      "  Sobrecalentamiento"
      "=>Temp Max Pileta   "
      "  Ajuste Sensor     "
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_4;

        escribo_display("  Anticongelamiento ", 1, 2);
        escribo_display("=>Sobrecalentamiento", 1, 3);
        escribo_display("  Temp Max Pileta   ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_7;

        escribo_display("  Temp Max Pileta   ", 1, 2);
        escribo_display("=>Ajuste Sensor     ", 1, 3);
        escribo_display("  Prog Diferencia   ", 1, 4);
      }
    break;

    case MODO_7:
      /*Muestra:
      "      AQUARIAL      "
      "  Temp Max Pileta   "
      "=>Ajuste Sensor     "
      "  Prog Diferencia   "
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_6;

        escribo_display("  Sobrecalentamiento", 1, 2);
        escribo_display("=>Temp Max Pileta   ", 1, 3);
        escribo_display("  Ajuste Sensor     ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_8;

        escribo_display("  Ajuste Sensor     ", 1, 2);
        escribo_display("=>Prog Diferencia   ", 1, 3);
        escribo_display("  Proteccion        ", 1, 4);
      }
    break;

    case MODO_8:
      /*Muestra:
      "      AQUARIAL      "
      "  Ajuste Sensor     "
      "=>Prog Diferencia   "
      "  Proteccion        "
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_7;

        escribo_display("  Temp Max Pileta   ", 1, 2);
        escribo_display("=>Ajuste Sensor     ", 1, 3);
        escribo_display("  Prog Diferencia   ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_9;

        escribo_display("  Prog Diferencia   ", 1, 2);
        escribo_display("=>Proteccion        ", 1, 3);
        escribo_display("  Regresar          ", 1, 4);
      }
    break;

    case MODO_9:
      /*Muestra:
      "      AQUARIAL      "
      "  Prog Diferencia   "
      "=>Proteccion        "
      "  Regresar          "
      */

      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_8;

        escribo_display("  Ajuste Sensor     ", 1, 2);
        escribo_display("=>Prog Diferencia   ", 1, 3);
        escribo_display("  Proteccion        ", 1, 4);
      }

      if (pulsadores == PULS_DIS)
      {
        menu_programacion = MODO_REGRESAR;

        escribo_display("  Proteccion        ", 1, 2);
        escribo_display("=>Regresar          ", 1, 3);
        escribo_display("                    ", 1, 4);
      }
    break;

    case MODO_REGRESAR:
      /*Muestra:
      "      AQUARIAL      "
      "  Proteccion        "
      "=>Regresar          "
      "                    "
      */
      if (pulsadores == PULS_AUM)
      {
        menu_programacion = MODO_9;

        escribo_display("  Prog Diferencia   ", 1, 2);
        escribo_display("=>Proteccion        ", 1, 3);
        escribo_display("  Regresar          ", 1, 4);
      }
    break;

    case MODO_1_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Dif Temp Bomba ON   "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        variables_config.dif_temp_on = subo_teclado(variables_config.dif_temp_on);                                //Hago una suba logaritmica al mantener apretado el pulsador

        if (variables_config.dif_temp_on > DIF_TEMP_ON) variables_config.dif_temp_on = DIF_TEMP_ON;               //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.dif_temp_on);
      }

      if (pulsadores == PULS_DIS)
      {
        variables_config.dif_temp_on = bajo_teclado(variables_config.dif_temp_on);

        if (variables_config.dif_temp_on <= variables_config.dif_temp_off) variables_config.dif_temp_on = variables_config.dif_temp_off+1;          //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.dif_temp_on);
      }
    break;

    case MODO_2_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Dif Temp Bomba OFF  "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        variables_config.dif_temp_off = subo_teclado(variables_config.dif_temp_off);                              //Hago una suba logaritmica al mantener apretado el pulsador

        if (variables_config.dif_temp_off >= variables_config.dif_temp_on) variables_config.dif_temp_off = variables_config.dif_temp_on-1;          //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.dif_temp_off);
      }

      if (pulsadores == PULS_DIS)
      {
        variables_config.dif_temp_off = bajo_teclado(variables_config.dif_temp_off);

        if (variables_config.dif_temp_off <= DIF_TEMP_OFF) variables_config.dif_temp_off = DIF_TEMP_OFF;          //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.dif_temp_off);
      }
    break;

    case MODO_3_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Anticongelamiento   "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        variables_config.anticong = subo_teclado(variables_config.anticong);                              //Hago una suba logaritmica al mantener apretado el pulsador

        if (variables_config.anticong >= TEMP_LIM_UP) variables_config.anticong = TEMP_LIM_UP;                    //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.anticong);
      }

      if (pulsadores == PULS_DIS)
      {
        variables_config.anticong = bajo_teclado(variables_config.anticong);

        if (variables_config.anticong <= TEMP_LIM_DOWN) variables_config.anticong = TEMP_LIM_DOWN;                //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.anticong);
      }
    break;

    case MODO_4_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Sobrecalentamiento  "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        if (cant_pas_up < 5) variables_config.sobrecal += 10;
        if ((cant_pas_up >= 5) && (cant_pas_up < 10)) variables_config.sobrecal += 20;
        if ((cant_pas_up >= 10) && (cant_pas_up < 20)) variables_config.sobrecal += 50;
        if ((cant_pas_up >= 20) && (cant_pas_up < 30)) variables_config.sobrecal += 100;
        if (cant_pas_up >= 30) variables_config.sobrecal += 500;

        if (variables_config.sobrecal >= TEMP_LIM_UP) variables_config.sobrecal = TEMP_LIM_UP;                    //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.sobrecal);
      }

      if (pulsadores == PULS_DIS)
      {
        if (cant_pas_down < 5)
        {
          if (variables_config.sobrecal <= 10) variables_config.sobrecal = 0;
          else                variables_config.sobrecal -= 10;
        }

        if ((cant_pas_down >= 5) && (cant_pas_down < 10))
        {
          if (variables_config.sobrecal <= 20) variables_config.sobrecal = 0;
          else                variables_config.sobrecal -= 20;
        }

        if ((cant_pas_down >= 10) && (cant_pas_down < 20))
        {
          if (variables_config.sobrecal <= 50) variables_config.sobrecal = 0;
          else                variables_config.sobrecal -= 50;
        }

        if ((cant_pas_down >= 20) && (cant_pas_down < 30))
        {
          if (variables_config.sobrecal <= 100) variables_config.sobrecal = 0;
          else                 variables_config.sobrecal -= 100;
        }

        if (cant_pas_down >= 30)
        {
          if (variables_config.sobrecal <= 500) variables_config.sobrecal = 0;
          else                 variables_config.sobrecal -= 500;
        }

        if (variables_config.sobrecal <= 10) variables_config.sobrecal = 0;                                       //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.sobrecal);
      }
    break;

    case MODO_6_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Temp Max Pileta     "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        variables_config.temp_max_pileta = subo_teclado(variables_config.temp_max_pileta);                              //Hago una suba logaritmica al mantener apretado el pulsador

        if (variables_config.temp_max_pileta >= TEMP_LIM_UP) variables_config.temp_max_pileta = TEMP_LIM_UP;      //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.temp_max_pileta);
      }

      if (pulsadores == PULS_DIS)
      {
        variables_config.temp_max_pileta = bajo_teclado(variables_config.temp_max_pileta);

        if (variables_config.temp_max_pileta <= TEMP_LIM_DOWN) variables_config.temp_max_pileta = TEMP_LIM_DOWN;  //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.temp_max_pileta);
      }

      if (variables_config.temp_pileta > variables_config.temp_max_pileta) variables_config.temp_pileta = variables_config.temp_max_pileta;
    break;

    case MODO_7_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Ajuste Temp Pileta  "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        if (cant_pas_up < 5) variables_config.offset_pileta++;
        if ((cant_pas_up >= 5) && (cant_pas_up < 10)) variables_config.offset_pileta += 2;
        if ((cant_pas_up >= 10) && (cant_pas_up < 20)) variables_config.offset_pileta += 5;
        if ((cant_pas_up >= 20) && (cant_pas_up < 30)) variables_config.offset_pileta += 10;
        if (cant_pas_up >= 30) variables_config.offset_pileta += 50;

        if (variables_config.offset_pileta >= OFFSET_UP) variables_config.offset_pileta = OFFSET_UP;              //Limite superior de la variable
        muestreo_offset(5, 4, variables_config.offset_pileta);
      }

      if (pulsadores == PULS_DIS)
      {
        if (cant_pas_down < 5) variables_config.offset_pileta--;
        if ((cant_pas_down >= 5) && (cant_pas_down < 10)) variables_config.offset_pileta -= 2;
        if ((cant_pas_down >= 10) && (cant_pas_down < 20)) variables_config.offset_pileta -= 5;
        if ((cant_pas_down >= 20) && (cant_pas_down < 30)) variables_config.offset_pileta -= 10;
        if (cant_pas_down >= 30) variables_config.offset_pileta -= 50;

        if (variables_config.offset_pileta <= OFFSET_DOWN) variables_config.offset_pileta = OFFSET_DOWN;          //Limite inferior de la variable
        muestreo_offset(5, 4, variables_config.offset_pileta);
      }
    break;

    case MODO_8_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Prog Diferencia     "
      "Set:                "
      */
      if (pulsadores == PULS_AUM)
      {
        variables_config.histeresis = subo_teclado(variables_config.histeresis);                              //Hago una suba logaritmica al mantener apretado el pulsador

        if (variables_config.histeresis >= OFFSET_UP) variables_config.histeresis = OFFSET_UP;                    //Limite superior de la variable
        muestreo_temperatura(5, 4, variables_config.histeresis);
      }

      if (pulsadores == PULS_DIS)
      {
        variables_config.histeresis = bajo_teclado(variables_config.histeresis);

        if (variables_config.histeresis <= TEMP_LIM_DOWN) variables_config.histeresis = TEMP_LIM_DOWN;            //Limite inferior de la variable
        muestreo_temperatura(5, 4, variables_config.histeresis);
      }
    break;

    case MODO_9_ENTRA:
      /*Muestra:
      "      AQUARIAL      "
      "                    "
      "Proteccion          "
      "Set:                "
      */
      if ((pulsadores == PULS_AUM) || (pulsadores == PULS_DIS))
      {
        if (variables_config.proteccion_manual == DESHABILITADO) variables_config.proteccion_manual = HABILITADO;
        else                                    variables_config.proteccion_manual = DESHABILITADO;

        muestreo_proteccion(5, 4, variables_config.proteccion_manual);
      }
    break;

    default:
      menu_programacion = MODO_NULO;
    break;
  }
}

